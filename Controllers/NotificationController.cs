﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace KibanaNotifications.Controllers
{
    public class ErrorLogForNotificationPurposes
    {
        public string Message { get; }
        public DateTime Timestamp { get; }
    }

    public class ErrorLogs
    {
        public List<ErrorLogForNotificationPurposes> Logs { get; set; }
    }
    
    [ApiController]
    [Route("[controller]")]
    public class NotificationController : ControllerBase
    {
        private AppSettings AppSettings { get; set; }
        
        public NotificationController(IOptions<AppSettings> settings)
        {
            AppSettings = settings.Value;
        }

        [HttpPost]
        public void Post(ErrorLogs logs)
        {
            //if (logs.Logs.First().Timestamp.AddMinutes(5) <= logs.Logs.Last().Timestamp) return;

            var message = new
            {
                text = $"Just checking if it's working - we received 100 errors. First from {logs.Logs.First().Timestamp}, last from {logs.Logs.Last().Timestamp}"
            };

            var json = JsonConvert.SerializeObject(message);

            using (WebClient client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.UploadString(AppSettings.ChannelUrl, json);
            }
        }
    }
}